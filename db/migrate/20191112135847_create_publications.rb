class CreatePublications < ActiveRecord::Migration[6.0]
  def change
    create_table :publications do |t|
      t.string :name
      t.date :published
      t.text :abstract

      t.references :people, null: false, foreign_key: true
      t.references :faculties, null: false, foreign_key: true

      t.timestamps
    end
  end
end
