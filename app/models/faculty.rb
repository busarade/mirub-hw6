class Faculty < ApplicationRecord
  has_many :publications # 1:N, název cíle píšu plurálně

  validates :name, presence: true
  validates :code, presence: true
end
