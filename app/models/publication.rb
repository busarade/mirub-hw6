class Publication < ApplicationRecord
  # non-owning strana 1:N relace
  # název cíle bude v singuláru - odkazuje na Person, ale píšu to malým
  belongs_to :person, optional: true # povinnost tohoto zajistí view a kontrolér
  belongs_to :faculty, optional: true # povinnost tohoto zajistí view a kontrolér

  validates :name, presence: true
end
