class Person < ApplicationRecord
  has_many :publications # 1:N, název cíle píšu plurálně

  # Další:
  # has_one <singular> - 1:1
  #
  # M:N:
  # has_many <plural>, through: <plural> - přes explicitní spojovací tabulku (do relace můžeme uvést relátor)
  # has_and_belongs_to_many <plural> - spojovací tabulka se vytvoří implicitně - uvádí se na obou stranách relace

  validates :name, presence: true
  validates :email, presence: true
  validates :username, presence: true

  validates :email, format: { with: /\A[a-z0-9]+@[a-z0-9]+\.[a-z]{2,3}\z/,
                              message: "Vložte platnou e-mailovou adresu." }
end
